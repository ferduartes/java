package br.com.alura.gerenciador.servlet;


import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter(urlPatterns = "/entrada")
public class MonitoramentoFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        long antes = System.currentTimeMillis();

        String acao = request.getParameter("acao");

        //executa
        chain.doFilter(request, response);

        long depois = System.currentTimeMillis();

        System.out.println("Tempo de execução da acao " + acao + ": " + (depois - antes));

    }

    @Override
    public void destroy() {

    }

}
